-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 29 Bulan Mei 2019 pada 10.21
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.1.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uneune_app`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `akun`
--

CREATE TABLE `akun` (
  `id_tukang` int(11) DEFAULT NULL,
  `id_customer` int(11) DEFAULT NULL,
  `username` varchar(16) NOT NULL,
  `password` text NOT NULL,
  `role` tinyint(1) NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `akun`
--

INSERT INTO `akun` (`id_tukang`, `id_customer`, `username`, `password`, `role`, `last_login`) VALUES
(NULL, NULL, '', '$2y$10$fpZIqwnTdeh9MqTRDEJCz.bnKMLMdlhme8.cOz7l9TddlKsAcm126', 0, '2019-05-24 17:08:45');

-- --------------------------------------------------------

--
-- Struktur dari tabel `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `name` char(32) NOT NULL,
  `email` varchar(32) NOT NULL,
  `telp` varchar(13) NOT NULL,
  `alamat` varchar(64) NOT NULL,
  `jenis_kelamin` varchar(64) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `customer`
--

INSERT INTO `customer` (`id`, `name`, `email`, `telp`, `alamat`, `jenis_kelamin`, `password`) VALUES
(1, 'Andreas', 'song@gmail.com', '', '', '0', ''),
(2, 'Yolanda', 'yobe@gmail.com', '', '', '0', ''),
(3, '', 'asdsad', '', '', '0', '$2y$10$IJBHBKqLXibs9TznSx2V0uOf9EvirqH5PrVs0ShFcJdbpf0N9IPwu'),
(4, '', 'sdsa', '', '', '0', '$2y$10$zVvTRwKJ8JSXz2StQ/h4wuS2Nn.b7F2YJVXq5JPROit217Tzur5dG'),
(5, '', 'sdsa', '', '', '0', '$2y$10$wW1TeXduS2MjjVwox3YpW.12XP9oNxZPdlmxhhAP1koBM69WJO.j2'),
(6, '', 'sdsa', '', '', '0', '$2y$10$GFttpn8dmAgZVjihOhMfa.j076FhfEgnRXZ3bIuHYl3Ij2ZAZsA.m'),
(7, '', 'sdsa', '', '', '0', '$2y$10$czfSLcMHq50TDLnYlM6.3eE2ClixOtMqMmrlfqCQn9ZvvyjO.rc6q'),
(8, '', 'sdsa', '', '', '0', '$2y$10$cgIT1UtwxMLiTX9PoyQVDOiXV1azGcnq4FKtpBrKipujD7oHc47MC'),
(9, '', 'sdsa', '', '', '0', '$2y$10$eyraW49SQgWs.J8dVrjMJuz6oSMWN5LZg1yGgWug7CqztfAvit7bu'),
(10, '', 'sdsa', '', '', '0', '$2y$10$qQCMdNjn1TWAr9Pw6Q6fe.tTD7QAx3e3aRIvPCjiFFBYTH4VeXqmW'),
(11, '', 'sdsa', '', '', '0', '$2y$10$aHtkhO2c303la35x/nKjbupMj69RW1.jQEoGud2up540SrSBxiT1i'),
(12, '', 'sdsa', '', '', '0', '$2y$10$UkJFwZ41sH0Q7YMo.dDOFuNTD8odtkGl4rTChVQGlGDf8LiGYZKxC'),
(13, '', 'sdsa', '', '', '0', '$2y$10$mexZL1GFdyqHY4/tbWiAXeFctrcJ4ObS9YpcW8edeZVTbbQ9yfHS6'),
(14, '', 'sdsa', '', '', '0', '$2y$10$T0HrAsC0igvw/HDL3MPqG.3YZpOLFW2BJxR3s.JPqtFpbJGb22PpC'),
(15, '', 'sdsa', '', '', '0', '$2y$10$OOFjZjiLPx8DS3TUZwBXMegF/YGJMFAcgLWgWUmmO3MZga2utAm4q'),
(16, '', 'asdsad', '', '', '0', '$2y$10$Zjf6vOV6ThgO2zvYedpsFu9CMD.YWgN5SSQ8U12mXNqeJ0mpdvYVe'),
(17, '', 'asdsad', '', '', '0', '$2y$10$tC3iVTTHFwbcs2RL.4XC.upRYYbqcEzjE2/Bufhb1KR/ghofylkKm'),
(18, '', 'asdad', '', '', '0', '$2y$10$aiNkRcPb6UPo7gs6pIOoT.wXuwCtzwaOdwZpttkIKNKOINLcrE4tK'),
(19, 'tina', 'tinabong', '8790980', 'hdjkvnkn', '0', '$2y$10$FYQVePX1ME2SF2wOfX2P5uZDhyWJqxeWhx2KCiQx2eH/UOdhqs6Qy'),
(20, 'tina', 'tinabong', '0101010', 'pi del', '0', '$2y$10$fVnEe0Pc6nDwx4vITQhAROsMCfeLszh9DDX6e0KBoAapyALCDVC3G'),
(21, 'tina', 'tinabongs', '9879898', 'hjkljkljk', '0', '$2y$10$uwlzabtFpjbW3zUGgXOVDOJYdHS8XuB2iK3SHkmbgL1VhEhFpChqq'),
(22, 'asda', 'asdsad', 'asdas', 'asda', '0', '$2y$10$QMiaHYOHVTCplPYFHjvrUe3bc9IlJXZuS9B4hhSk0BCDGgjIIfWsC'),
(23, 'fsdf', 'sdfsd', 'sdfds', 'sdfsd', '0', '$2y$10$QYy8u.hdlXEZLGgZp/RCcecNg6iQuew2cHtVlrADEtAZCewZdHcey'),
(24, 'dsfdsf', 'sdfsd', 'sdfsd', 'sdfsd', '0', '$2y$10$f56oeRr2yjEUuMqro7TjQO96E6ybyMqZO7bY4KLTGibWmxdQ3vN9.'),
(25, 'sdasd', 'asdasd', 'asdsad', 'asdsad', '0', '$2y$10$8V5moBo28Pos2KTV2DOw3uK4qHcGhxhPmh7REmakYd9MIQx2daKDe'),
(26, 'fdsf', 'dfgfdg', 'fdgfdg', 'dfgfd', '0', '$2y$10$FewvYpJZh8Y25ANJyX4ufOm0nM2ir91zIyxiNjftHxJGRee7hjPQ6'),
(27, 'sadasd', 'andreas', '6756', 'medan', 'dfdf', '$2y$10$wV9fz1TzjRDnYpkBj/eNguKZkIdlzwJ7xSDw71X2XNEPP4gdPkRO.'),
(28, 'andreas', 'loping@gmail.com', '081343', 'medan', '', '$2y$10$bGr.4n3YzJ7aBtt5kcUT/eWJhHDtSKZCirDbzOdOP0AyeF7Siz8g6');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `nama_kategori` char(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id`, `nama_kategori`) VALUES
(4, ''),
(1, 'AC'),
(3, 'TV'),
(2, 'WC');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelola_pesanan`
--

CREATE TABLE `kelola_pesanan` (
  `id_customer` int(11) NOT NULL,
  `id_pemesanan` int(11) NOT NULL,
  `status_pemesanan` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pesanan`
--

CREATE TABLE `pesanan` (
  `id` int(11) NOT NULL,
  `keterangan_kerusakan` varchar(150) NOT NULL,
  `alamat` varchar(64) NOT NULL,
  `status` varchar(100) NOT NULL,
  `kategori` varchar(20) NOT NULL,
  `waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pesanan`
--

INSERT INTO `pesanan` (`id`, `keterangan_kerusakan`, `alamat`, `status`, `kategori`, `waktu`) VALUES
(23, 'asdsad', 'asdasd', 'asdasd', 'asdas', '2019-05-26 08:17:21'),
(24, 'asdsadsa', 'fghfhgf', 'ghjgfh', 'hjhjhg', '2019-05-26 08:18:06'),
(25, '', 'sdfdssdfsd', 'fdsdsf', 'sdfdsf', '2019-05-26 08:20:06'),
(26, '', 'sdfdssdfsd', 'fdsdsf', 'sdfdsf', '2019-05-26 08:21:36'),
(27, 'dfds', 'dsfdsf', 'sdfsdf', 'sdfsdf', '2019-05-26 08:22:34'),
(28, 'dsfsf', 'asdadas', 'asdas', 'asdasdas', '2019-05-26 08:26:52'),
(29, 'sdfsdf', 'sdfds', 'sdfsd', '', '2019-05-26 08:32:56'),
(30, 'sdfdsf', 'sdfdsf', 'sdfdsf', 'sdfsdf', '2019-05-26 08:34:07'),
(31, 'asdfs', 'sadsa', 'asdas', 'asd', '2019-05-26 08:44:28'),
(32, 'sdad', 'asdsad', 'sdfdsf', 'dfdsf', '2019-05-26 10:03:37');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pilihan_kategori`
--

CREATE TABLE `pilihan_kategori` (
  `id` int(11) NOT NULL,
  `id_tukang` int(11) NOT NULL,
  `nama_kategori` char(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pilihan_kategori`
--

INSERT INTO `pilihan_kategori` (`id`, `id_tukang`, `nama_kategori`) VALUES
(1, 1, 'AC');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rating`
--

CREATE TABLE `rating` (
  `id_rating` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `id_tukang` int(11) NOT NULL,
  `jumlah_rating` int(11) DEFAULT NULL,
  `komentar` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `rating`
--

INSERT INTO `rating` (`id_rating`, `id_customer`, `id_tukang`, `jumlah_rating`, `komentar`) VALUES
(1, 2, 1, 10, 'Bagus');

-- --------------------------------------------------------

--
-- Struktur dari tabel `respon`
--

CREATE TABLE `respon` (
  `id_tukang` int(11) NOT NULL,
  `id_pemesanan` int(11) NOT NULL,
  `Respon` char(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `respon`
--

INSERT INTO `respon` (`id_tukang`, `id_pemesanan`, `Respon`) VALUES
(2, 1, 'Terima');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tukang`
--

CREATE TABLE `tukang` (
  `id` int(11) NOT NULL,
  `name` char(64) NOT NULL,
  `email` char(64) NOT NULL,
  `telp` varchar(13) NOT NULL,
  `alamat` varchar(64) NOT NULL,
  `jenis_kelamin` tinyint(1) NOT NULL,
  `keahlian` char(16) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tukang`
--

INSERT INTO `tukang` (`id`, `name`, `email`, `telp`, `alamat`, `jenis_kelamin`, `keahlian`, `password`) VALUES
(1, 'Kaleb', 'L', 'PI Del', '081298907879', 0, 'AC', ''),
(2, 'Ventina', 'P', 'PI Del', '081234567890', 0, 'TV', ''),
(6, 'sdsad', 'asdsad', 'asdsa', 'asdasda', 0, 'asdasdsa', '$2y$10$fP6VqiAbgWdaWgY4TQFPv.dAjxn0.GvkGQzZrlvSnGwiiQlqEalB.'),
(7, 'Nama', 'nama@del.id', '081208120812', 'tarutung', 0, 'AC', '$2y$10$IbFJYPDWAPX1FgHjLWs2mOzGqI6KTer0Ry1WMdDldOkTShE7Gds6G'),
(8, 'Nama', 'nama@del.id', '081208120812', 'tarutung', 0, 'AC', '$2y$10$.o.4GxQ3ZvkLbXB3diCbxO.QEgro/9pdgyfz7KG6L00WEmk/rbmRC'),
(9, 'Nama', 'nama@del.id', '081208120812', 'tarutung', 0, 'AC', '$2y$10$EBxs9sye8W3pwMAjJK7TX.cS6fB0QYgS5snG3VDZg1p8ldFGZ9rHO'),
(10, 'Nama', 'nama@del.id', '081208120812', 'tarutung', 0, 'AC', '$2y$10$BvM92c9GaHndNSKF2mBg2Obf0CMQbw8lvAllRr4ljKlhVUBdJauw.'),
(11, 'asdas', 'sadasd', '675675', 'gdfgdf', 0, 'dfgdfg', '$2y$10$4uwN1vAHcmlxNZ61Y89WRu14mivEFv1qFBKdoipmycXjLkWCmnA/O'),
(12, 'sdasd', 'sdasd', '67657567', 'fhgfh', 0, 'gjhjh', '$2y$10$0C4FkDQsNdWu6bKL3PhP1.XiZhdUdUdOK71aihIflai05BFt6/RWq');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `akun`
--
ALTER TABLE `akun`
  ADD PRIMARY KEY (`username`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `id_tukang` (`id_tukang`);

--
-- Indeks untuk tabel `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nama_kategori` (`nama_kategori`);

--
-- Indeks untuk tabel `kelola_pesanan`
--
ALTER TABLE `kelola_pesanan`
  ADD PRIMARY KEY (`id_customer`),
  ADD KEY `id_pemesanan` (`id_pemesanan`);

--
-- Indeks untuk tabel `pesanan`
--
ALTER TABLE `pesanan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pilihan_kategori`
--
ALTER TABLE `pilihan_kategori`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nama_kategori` (`nama_kategori`),
  ADD KEY `id_tukang` (`id_tukang`);

--
-- Indeks untuk tabel `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id_rating`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `id_tukang` (`id_tukang`);

--
-- Indeks untuk tabel `respon`
--
ALTER TABLE `respon`
  ADD PRIMARY KEY (`id_pemesanan`) USING BTREE,
  ADD KEY `id_tukang` (`id_tukang`);

--
-- Indeks untuk tabel `tukang`
--
ALTER TABLE `tukang`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `pesanan`
--
ALTER TABLE `pesanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT untuk tabel `pilihan_kategori`
--
ALTER TABLE `pilihan_kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `rating`
--
ALTER TABLE `rating`
  MODIFY `id_rating` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tukang`
--
ALTER TABLE `tukang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `akun`
--
ALTER TABLE `akun`
  ADD CONSTRAINT `akun_ibfk_1` FOREIGN KEY (`id_customer`) REFERENCES `customer` (`id`),
  ADD CONSTRAINT `akun_ibfk_2` FOREIGN KEY (`id_tukang`) REFERENCES `tukang` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
