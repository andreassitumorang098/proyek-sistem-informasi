package com.psi1908.uneuneapp;

import android.app.TimePickerDialog;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;

public class OrderACActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener{

    public static TextView text_date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_ac);

        text_date = findViewById(R.id.text_date);

        Button btn_time = findViewById(R.id.btn_time);
        btn_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment dialogFragment = new TimePickerFragment();
                dialogFragment.show(getSupportFragmentManager(), "Jam = ");
            }
        });

    }

    public void button_date(View view) {

        DialogFragment fragment = new MyDateFragment2();
        fragment.show(getSupportFragmentManager(), "Tanggal = ");
    }

    public static void PopulateSetDateText (int year, int month, int day){

        text_date.setText(year + "/" + month + "/" + day);
    }

    @Override
    public void onTimeSet(TimePicker view, int hour, int minute) {
        TextView text_time;
        text_time = findViewById(R.id.text_time);
        text_time.setText(hour + " : " + minute);
    }
}