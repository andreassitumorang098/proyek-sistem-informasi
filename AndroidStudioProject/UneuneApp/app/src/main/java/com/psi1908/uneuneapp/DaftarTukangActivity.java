package com.psi1908.uneuneapp;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class DaftarTukangActivity extends AppCompatActivity {

    ListView list;
    String titles[] = {"Herman sitorus", "Dion Situmorang", "Ratna Sarumpaet", "Maria Sigalingging", "Manahan manihururk", "Ventina limbong", "Hotma Sagala"};
    String descriptions[] = {"Pipa Air dan Elektronik", "Pipa Air", "AC dan Pipa Air", "Atap dan Pipa Air", "Atap dan Pipa Air", "Pipa Air", "Pengecetan dan Pipa Air"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_tukang);

        list = findViewById(R.id.list1);

        DaftarTukangActivity.MyAdapter adapter = new DaftarTukangActivity.MyAdapter(this, titles, descriptions);

        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0){
                    Intent intent = new Intent(DaftarTukangActivity.this, OrderPipaairActivity.class);
                    startActivity(intent);
                }
                if (position == 1){
                    Intent intent = new Intent(DaftarTukangActivity.this, OrderPipaairActivity.class);
                    startActivity(intent);
                }
                if (position == 2){
                    Intent intent = new Intent(DaftarTukangActivity.this, OrderPipaairActivity.class);
                    startActivity(intent);
                }
                if (position == 3){
                    Intent intent = new Intent(DaftarTukangActivity.this, OrderPipaairActivity.class);
                    startActivity(intent);
                }
                if (position == 4){
                    Intent intent = new Intent(DaftarTukangActivity.this, OrderPipaairActivity.class);
                    startActivity(intent);
                }
                if (position == 5){
                    Intent intent = new Intent(DaftarTukangActivity.this, OrderPipaairActivity.class);
                    startActivity(intent);
                }
                if (position == 6){
                    Intent intent = new Intent(DaftarTukangActivity.this, OrderPipaairActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    class MyAdapter extends ArrayAdapter<String> {
        Context context;
        String myTitles[];
        String myDescriptions[];

        MyAdapter(Context context, String[] titles, String[] descriptions){
            super(context,R.layout.row1, R.id.text1, titles);
            this.context=context;
            this.myTitles=titles;
            this.myDescriptions=descriptions;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(context.LAYOUT_INFLATER_SERVICE);
            View row1 = layoutInflater.inflate(R.layout.row1, parent, false);
            TextView myTitles = row1.findViewById(R.id.text1);
            TextView myDescription = row1.findViewById(R.id.text2);
            myTitles.setText(titles[position]);
            myDescription.setText(descriptions[position]);

            return row1;
        }
    }
}