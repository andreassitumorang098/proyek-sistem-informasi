package com.psi1908.uneuneapp;

import android.annotation.SuppressLint;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

public class OrderPipaairActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener{

    @SuppressLint("StaticFieldLeak")
    public static TextView text_date;

    Button btn_submit, btn_tukang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orderpipaair);

        btn_submit = findViewById(R.id.btn_submit);
        btn_tukang = findViewById(R.id.btn_tukang);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OrderPipaairActivity.this, DetailOrderActivity.class));
            }
        });

        btn_tukang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OrderPipaairActivity.this, DaftarTukangActivity.class));
            }
        });

        text_date = findViewById(R.id.text_date);

        Button btn_time = findViewById(R.id.btn_time);
        btn_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment dialogFragment = new TimePickerFragment();
                dialogFragment.show(getSupportFragmentManager(), "Jam = ");
            }
        });

    }

    public void button_date(View view) {

        DialogFragment fragment = new MyDateFragment1();
        fragment.show(getSupportFragmentManager(), "Tanggal = ");
    }

    public static void PopulateSetDateText (int year, int month, int day){

        text_date.setText(year + " / " + month + " / " + day);
    }

    @Override
    public void onTimeSet(TimePicker view, int hour, int minute) {
        TextView text_time;
        text_time = findViewById(R.id.text_time);
        text_time.setText(hour + " : " + minute);
    }
}