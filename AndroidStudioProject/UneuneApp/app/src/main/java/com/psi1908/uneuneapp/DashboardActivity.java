package com.psi1908.uneuneapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.smarteist.autoimageslider.DefaultSliderView;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderLayout;
import com.smarteist.autoimageslider.SliderView;

public class DashboardActivity extends AppCompatActivity {

    private ImageView pipaair, ac, perabot, elektronik, atap, pengecatan;


    SliderLayout sliderLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        pipaair = findViewById(R.id.pipaair);
        ac = findViewById(R.id.ac);
        perabot = findViewById(R.id.perabot);
        elektronik = findViewById(R.id.elektronik);
        atap = findViewById(R.id.atap);
        pengecatan = findViewById(R.id.pengecatan);


        pipaair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this, OrderPipaairActivity.class);
                startActivity(intent);
            }
        });

        ac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this, OrderACActivity.class);
                startActivity(intent);
            }
        });

        perabot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this, OrderPerabotActivity.class);
                startActivity(intent);
            }
        });

        elektronik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this, OrderElektronikActivity.class);
                startActivity(intent);
            }
        });

        atap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this, OrderAtapActivity.class);
                startActivity(intent);
            }
        });

        pengecatan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this, OrderPengecatanActivity.class);
                startActivity(intent);
            }
        });


        sliderLayout = findViewById(R.id.imageSlider);
        sliderLayout.setIndicatorAnimation(IndicatorAnimations.FILL); //set indicator animation by using SliderLayout.Animations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderLayout.setScrollTimeInSec(1); //set scroll delay in seconds :

        setSliderViews();

    }

    private void setSliderViews() {

        for (int i = 0; i <= 4; i++) {

            DefaultSliderView sliderView = new DefaultSliderView(this);

            switch (i) {
                case 0:
                    sliderView.setImageDrawable(R.drawable.img2);
                    break;
                case 1:
                    sliderView.setImageDrawable(R.drawable.img1);
                    break;
                case 2:
                    sliderView.setImageDrawable(R.drawable.img3);
                    break;
                case 3:
                    sliderView.setImageDrawable(R.drawable.img4);
                    break;
                case 4:
                    sliderView.setImageDrawable(R.drawable.img5);
                    break;
            }

            sliderView.setImageScaleType(ImageView.ScaleType.CENTER_CROP);
            final int finalI = i;
            sliderView.setOnSliderClickListener(new SliderView.OnSliderClickListener() {
                @Override
                public void onSliderClick(SliderView sliderView) {
                    Toast.makeText(DashboardActivity.this, "This is slider " + (finalI + 1), Toast.LENGTH_SHORT).show();
                }
            });

            //at last add this view in your layout :
            sliderLayout.addSliderView(sliderView);
        }
    }
}






