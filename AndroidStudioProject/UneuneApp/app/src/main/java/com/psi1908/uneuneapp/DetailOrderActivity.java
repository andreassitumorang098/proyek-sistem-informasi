package com.psi1908.uneuneapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DetailOrderActivity extends AppCompatActivity {

    Button btn_done;
    TextView textViewA;
    TextView textViewB;
    TextView textViewC;
    TextView textViewD;
    TextView textViewE;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailorder);


        btn_done = findViewById(R.id.btn_done);
        textViewE = findViewById(R.id.tukang);
        textViewD =  findViewById(R.id.deskripsi);
        textViewC =  findViewById(R.id.text_time);
        textViewB =  findViewById(R.id.text_date);
        textViewA =  findViewById(R.id.address);

        textViewE.setText("Hotma Sagala");
        textViewD.setText("pipa pecah dan berkarat serta bocor");
        textViewC.setText("08.35");
        textViewB.setText("13/06/2019");
        textViewA.setText("jl.jermal 3 bangun sari 4 medan");

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DetailOrderActivity.this, DoneActivity.class));
            }
        });
    }
}
