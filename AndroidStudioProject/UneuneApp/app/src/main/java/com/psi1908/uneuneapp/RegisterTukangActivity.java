package com.psi1908.uneuneapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterTukangActivity extends AppCompatActivity {
    private EditText name, email, password, confirm_password, address, telp, ability, gender;
    private Button btn_registTukang;
    private ProgressBar loading;
    private static String URL_REGIST = "http://192.168.60.1/android_register_login/register_tukang.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registertukang);

        loading = findViewById(R.id.loading);
        name = findViewById(R.id.name);
        email = findViewById(R.id.email);
        telp = findViewById(R.id.telp);
        address = findViewById(R.id.address);
        gender = findViewById(R.id.gender);
        ability = findViewById(R.id.Ability);
        password = findViewById(R.id.password);
        confirm_password = findViewById(R.id.confirm_password);
        btn_registTukang = findViewById(R.id.btn_registTukang);

        btn_registTukang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Regist();
            }
        });

    }

    private void Regist() {
        loading.setVisibility(View.VISIBLE);
        btn_registTukang.setVisibility(View.GONE);

        final String name = this.name.getText().toString().trim();
        final String email = this.email.getText().toString().trim();
        final String telp = this.telp.getText().toString().trim();
        final String address = this.address.getText().toString().trim();
        final String gender = this.gender.getText().toString().trim();
        final String ability = this.ability.getText().toString().trim();
        final String password = this.password.getText().toString().trim();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_REGIST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");

                            if (success.equals("1")) {
                                Toast.makeText(RegisterTukangActivity.this, "Register Success!", Toast.LENGTH_SHORT).show();

                                Intent intent = new Intent(RegisterTukangActivity.this, LoginActivity.class);
                                startActivity(intent);
                                finish();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(RegisterTukangActivity.this, "Register Error! " + e.toString(), Toast.LENGTH_SHORT).show();
                            loading.setVisibility(View.GONE);
                            btn_registTukang.setVisibility(View.VISIBLE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(RegisterTukangActivity.this, "Register Error! " + error.toString(), Toast.LENGTH_SHORT).show();
                        loading.setVisibility(View.GONE);
                        btn_registTukang.setVisibility(View.VISIBLE);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("name", name);
                params.put("email", email);
                params.put("telp", telp);
                params.put("alamat", address);
                params.put("jenis_kelamin", gender);
                params.put("keahlian", ability);
                params.put("password", password);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);


    }
}