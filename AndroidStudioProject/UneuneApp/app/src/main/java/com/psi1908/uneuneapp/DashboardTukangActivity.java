package com.psi1908.uneuneapp;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class DashboardTukangActivity extends AppCompatActivity {

    ListView list;
    String titles[] = {"Title One", "Title two", "Title three", "Title Four", "Title Five"};
    String descriptions[] = {"Desc 1 ...", "Desc 2 ...", "Desc 3 ...", "Desc 4 ...", "Desc 5 ..."};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_tukang);

        list = findViewById(R.id.list1);

        MyAdapter adapter = new MyAdapter(this, titles, descriptions);

        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0){
                    Intent intent = new Intent(DashboardTukangActivity.this, DetailPesananMasukActivity.class);
                    startActivity(intent);
                }

                if (position == 1){
                    Toast.makeText(DashboardTukangActivity.this, "Item 2 clicked", Toast.LENGTH_SHORT).show();
                }

                if (position == 2){
                    Toast.makeText(DashboardTukangActivity.this, "Item 3 clicked", Toast.LENGTH_SHORT).show();
                }
                if (position == 3){
                    Toast.makeText(DashboardTukangActivity.this, "Item 4 clicked", Toast.LENGTH_SHORT).show();
                }

                if (position == 4){
                    Toast.makeText(DashboardTukangActivity.this, "Item 5 clicked", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    class MyAdapter extends ArrayAdapter<String>{
        Context context;
        String myTitles[];
        String myDescriptions[];

        MyAdapter(Context context, String[] titles, String[] descriptions){
            super(context,R.layout.row, R.id.text1, titles);
            this.context=context;
            this.myTitles=titles;
            this.myDescriptions=descriptions;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.row, parent, false);
            TextView myTitles = row.findViewById(R.id.text1);
            TextView myDescription = row.findViewById(R.id.text2);
            myTitles.setText(titles[position]);
            myDescription.setText(descriptions[position]);

            return row;
        }
    }
}
